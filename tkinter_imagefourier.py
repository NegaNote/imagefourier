#!/home/athena/anaconda3/bin/python3.7
from imagefourier import get_coeffs_from_image, fourierfunc
import numpy as np
import struct
import os
import sys
import traceback
import tkinter as tk
import tkinter.scrolledtext as st
from _tkinter import TclError
from tkinter.filedialog import askopenfilename, asksaveasfilename
from xml.parsers.expat import ExpatError
import matplotlib
matplotlib.use('TkAgg')  # analysis:ignore
import matplotlib.backends.backend_tkagg as tkagg  # analysis:ignore
from matplotlib.figure import Figure  # analysis:ignore


class LoggingScrolledText(st.ScrolledText):
    def __init__(self, master=None, *args, **kwargs):
        super().__init__(master, *args, **kwargs)
        self["state"] = tk.DISABLED

    def insert(self, index, c, *args):
        self["state"] = tk.NORMAL
        super().insert(index, c, *args)
        self.see(tk.END)
        self["state"] = tk.DISABLED


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.current_coeffs = None
        self.pack()
        self.create_widgets()

    def _retrieve_image_fname(self):
        fname = askopenfilename(title="Select an image")
        if not fname:
            return
        else:
            self.image_fname_label_content.set(fname)

    def _read_coeffs_from_file(self):
        fname = askopenfilename(title="Select a file to read coeffs from")
        if not fname:
            return
        with open(fname, 'rb') as f:
            temp_coeffs = {}
            try:
                binary_data = f.read()
                coeff_iterator = struct.iter_unpack('<ldd', binary_data)
                for index, coeff_real, coeff_imag in coeff_iterator:
                    temp_coeffs[index] = complex(coeff_real, coeff_imag)
            except Exception:
                self.logs_scrolledtext.insert(
                        tk.END,
                        "Invalid file format.\n")
            else:
                self.current_coeffs = temp_coeffs
                self.logs_scrolledtext.insert(
                        tk.END,
                        f"Successfully read coefficients from {fname}.\n")

    def _save_coeffs_to_file(self):
        fname = asksaveasfilename(title="Select a file to save coeffs to")
        if not fname:
            return
        with open(fname, 'wb') as f:
            for index, coeff in self.current_coeffs.items():
                data = struct.pack('<ldd', index, coeff.real, coeff.imag)
                f.write(data)
        self.logs_scrolledtext.insert(
                tk.END,
                f"Saved coeffs to {fname}.\n")
        pass

    def _log_coefficients(self):
        fname = self.image_fname_label_content.get()
        if len(fname) <= 0:
            self.logs_scrolledtext.insert(
                    tk.END,
                    "No file selected.\n")
        else:
            self.logs_scrolledtext.insert(
                    tk.END,
                    "Starting calculations...\n")
            try:
                if not (self.ignore_precalc_coeffs_var.get()
                        and self.current_coeffs):
                    min_coeff = self.min_coeff_entry_content.get()
                    max_coeff = self.max_coeff_entry_content.get()
                    self.current_coeffs = get_coeffs_from_image(
                        fname,
                        textwidget=self.logs_scrolledtext,
                        min_coeff=min_coeff,
                        max_coeff=max_coeff)
                else:
                    curr_min = min(self.current_coeffs.keys())
                    curr_max = max(self.current_coeffs.keys())
                    d1 = {}
                    d2 = {}
                    if self.min_coeff_entry_content.get() < curr_min:
                        d1 = get_coeffs_from_image(
                                fname,
                                textwidget=self.logs_scrolledtext,
                                min_coeff=self.min_coeff_entry_content.get(),
                                max_coeff=curr_min - 1)
                    if self.max_coeff_entry_content.get() > curr_max:
                        d2 = get_coeffs_from_image(
                                fname,
                                textwidget=self.logs_scrolledtext,
                                min_coeff=curr_max + 1,
                                max_coeff=self.max_coeff_entry_content.get())
                    d3 = d2
                    d3.update(d1)
                    d3.update(self.current_coeffs)
                    self.current_coeffs = d3
                    del d1
                    del d2
                    del d3
            except IOError:
                self.logs_scrolledtext.insert(
                        tk.END,
                        "Invalid file format.\n")
            except ExpatError:
                self.logs_scrolledtext.insert(
                        tk.END,
                        "Invalid file format.\n")
            except TclError:
                self.logs_scrolledtext.insert(
                        tk.END,
                        "One or more options has a non-integer value.\n")
            except Exception:
                self.logs_scrolledtext.insert(
                        tk.END,
                        "An exception has occurred: "
                        + traceback.format_exc()
                        + "\n")
            else:
                self.logs_scrolledtext.insert(
                        tk.END,
                        "Finished calculations.\n")

    def _quit(self):
        self.master.quit()
        self.master.destroy()

    def _draw_plot(self):
        if self.current_coeffs:
            t_range = np.linspace(0, 2*np.pi, 10000)
            fourier_t = fourierfunc(t_range, self.current_coeffs)
            fourier_t_real = []
            for val in fourier_t:
                fourier_t_real.append(val.real)
            fourier_t_imag = []
            for val in fourier_t:
                fourier_t_imag.append(val.imag)
            self.subplot.cla()
            self.subplot.plot(fourier_t_real, fourier_t_imag)
            self.subplot.axis("scaled")
            self.fig.canvas.draw()
            self.canvas.draw()
        else:
            self.logs_scrolledtext.insert(
                    tk.END,
                    "No coefficients have been calculated yet.\n")

    def create_widgets(self):
        self.right_frame = tk.Frame(self)
        self.right_frame.grid(column=1, row=0, padx=10, pady=5)

        self.coeff_frame = tk.Frame(self.right_frame)
        self.coeff_frame.grid(row=0, column=0)

        self.ignore_precalc_coeffs_var = tk.BooleanVar()
        self.ignore_precalc_coeffs_var.set(False)

        self.ignore_precalc_coeffs_chk = tk.Checkbutton(
                self.coeff_frame,
                text="Skip calculating already-known coefficients",
                variable=self.ignore_precalc_coeffs_var)
        self.ignore_precalc_coeffs_chk.grid(row=0, pady=5)

        self.calc_button = tk.Button(
                self.coeff_frame,
                text="Calculate fourier coefficients from image",
                command=self._log_coefficients)
        self.calc_button.grid(row=1, pady=10)

        self.logs_scrolledtext = LoggingScrolledText(
                self.coeff_frame,
                width=60,
                height=25)
        self.logs_scrolledtext.grid(row=2)

        self.options_frame = tk.Frame(self.right_frame)
        self.options_frame.grid(row=2, column=0)

        self.min_coeff_label = tk.Label(
                self.options_frame,
                text="Minimum coefficient index: ")
        self.min_coeff_label.grid(row=0, column=0)
        self.min_coeff_entry_content = tk.IntVar()
        self.min_coeff_entry_content.set(-5)
        self.min_coeff_entry = tk.Entry(
                self.options_frame,
                width=4,
                textvariable=self.min_coeff_entry_content)
        self.min_coeff_entry.grid(row=0, column=1)

        self.max_coeff_label = tk.Label(
                self.options_frame,
                text="Maximum coefficient index: ")
        self.max_coeff_label.grid(row=1, column=0)
        self.max_coeff_entry_content = tk.IntVar()
        self.max_coeff_entry_content.set(5)
        self.max_coeff_entry = tk.Entry(
                self.options_frame,
                width=4,
                textvariable=self.max_coeff_entry_content)
        self.max_coeff_entry.grid(row=1, column=1)

        self.read_coeffs_button = tk.Button(
                self.options_frame,
                text="Read coefficients from file...",
                command=self._read_coeffs_from_file)
        self.read_coeffs_button.grid(row=2, columnspan=2)

        self.save_coeffs_button = tk.Button(
                self.options_frame,
                text="Save coefficients to file...",
                command=self._save_coeffs_to_file)
        self.save_coeffs_button.grid(row=3, columnspan=2)

        self.left_frame = tk.Frame(self)
        self.left_frame.grid(column=0, row=0, padx=10)

        self.fname_frame = tk.Frame(self.left_frame)
        self.fname_frame.grid(row=0, pady=5)

        self.image_fname_browse_button = tk.Button(
                self.fname_frame,
                text="Browse for an image",
                command=self._retrieve_image_fname)
        self.image_fname_browse_button.grid(column=0, row=0)

        self.image_fname_label_content = tk.StringVar()
        self.image_fname_label = tk.Label(
                self.fname_frame,
                textvariable=self.image_fname_label_content,
                width=80)
        self.image_fname_label.grid(column=1, row=0)

        self.canvas_frame = tk.Frame(self.left_frame)
        self.canvas_frame.grid(row=1, column=0)

        self.fig = Figure(figsize=(5.0, 5.0))
        self.subplot = self.fig.add_subplot(111)
        self.plot_button = tk.Button(
                self.canvas_frame,
                text="Plot from coefficients",
                command=self._draw_plot)
        self.plot_button.grid(row=0, column=0, pady=10)

        self.canvas = tkagg.FigureCanvasTkAgg(
                self.fig,
                master=self.canvas_frame)
        self.canvas.get_tk_widget().grid(row=1, column=0)
        self.canvas.draw()

        self.quit_button = tk.Button(self, text="Quit",
                                     command=self._quit)
        self.quit_button.grid(
                row=1,
                columnspan=2,
                ipadx=150,
                ipady=15,
                pady=10)


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


root = tk.Tk()
root.title("Fourier coefficients from images")
root.tk.call('wm', 'iconphoto', root._w, tk.PhotoImage(
        file=resource_path('icon.gif')))
app = Application(master=root)
app.mainloop()
